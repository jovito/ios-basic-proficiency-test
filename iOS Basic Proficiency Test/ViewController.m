//
//  ViewController.m
//  iOS Basic Proficiency Test
//
//  Created by Jovito Royeca on 15/06/2017.
//  Copyright © 2017 Jovito Royeca. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
