//
//  main.m
//  iOS Basic Proficiency Test
//
//  Created by Jovito Royeca on 15/06/2017.
//  Copyright © 2017 Jovito Royeca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
