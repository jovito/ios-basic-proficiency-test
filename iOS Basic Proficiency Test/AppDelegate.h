//
//  AppDelegate.h
//  iOS Basic Proficiency Test
//
//  Created by Jovito Royeca on 15/06/2017.
//  Copyright © 2017 Jovito Royeca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CharactersViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

